<?php
$submitUrl = "?controller=users&action=edit";
?>

<form action="<?php echo $submitUrl; ?>" enctype=”multipart/form-data” method="post">
    <table>
        <tr>
            <th>First name</th>
            <th>Last name</th>
        </tr>
        <?php foreach ($users as $index => $user):?>
        <tr>
            <td><input type="text" name="users[<?php echo $index; ?>][firstname]" value="<?php echo $user->getFirstname()?>" /></td>
            <td><input type="text" name="users[<?php echo $index; ?>][surname]" value="<?php echo $user->getSurname() ?>" /></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <input type="submit" value="OK" />
</form>
