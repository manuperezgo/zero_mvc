<?php

final class Users
{
    /** @var  User[] */
    private $users;

    private $storage_file = __DIR__."/../storage/users.data";

    private static $instance = null;

    public static function getInstance(){
        if(self::$instance == null){
            self::$instance = new Users();
        }
        return self::$instance;
    }

    public function __construct(){
        $this->load();
    }

    private function load(){
        $users = [];
        if(file_exists($this->storage_file)){
            $serialized_data = file_get_contents($this->storage_file);
            $users = $serialized_data != "" ? unserialize($serialized_data) : [];
        }
        $this->users = $users;
        return $this;
    }

    public function save(){
        $serialized_data = serialize($this->users);
        file_put_contents($this->storage_file, $serialized_data);
        return $this;
    }

    /**
     * @param array $users
     * @return $this
     */
    public function setUsers(array $users){
        $this->users = $users;
        return $this;
    }

    /**
     * @return  User[]
     */
    public function getUsers(){
        return $this->users;
    }

}