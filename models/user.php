<?php

final class User
{
    /** @var  string firstname */
    private $firstname;
    /** @var  string surname */
    private $surname;

    /**
     * User constructor.
     *
     * @param string $firstname
     * @param string $surname
     */
    public function __construct($firstname, $surname)
    {
        $this->firstname = $firstname;
        $this->surname   = $surname;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    public function isValid(){

        return !empty($this->firstname) && !empty($this->surname) ?: false;
    }

}