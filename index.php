<?php
require_once(__DIR__ . "/lib/bootstrap.php");
$currentUrl = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];

    call($controller, $action);
} else if ($currentUrl == "/"){
    call("users", "index");
}
// for the moment, force index function

function call($controller, $action) {
    require_once('controllers/' . $controller . '_controller.php');

    switch($controller) {
        case 'users':
            $controller = new UsersController();
            break;
    }

    $controller->{ $action }();
}


?>