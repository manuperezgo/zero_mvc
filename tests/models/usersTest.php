<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers Users
 */
final class UsersTest extends TestCase
{
    /** @var  Users users */
    private $users;

    protected function setUp()
    {
        $this->users = Users::getInstance();
        parent::setUp();
    }

    public function testCanGetSetUsers()
    {
        $usersFixture = [];
        $usersFixture[] = ['firstname' => 'name1', 'surname' => 'surname1'];
        $usersFixture[] = ['firstname' => 'name2', 'surname' => 'surname2'];
        $usersFixture[] = ['firstname' => 'name3', 'surname' => 'surname3'];

        $this->users->setUsers($usersFixture);
        $this->assertEquals($usersFixture, $this->users->getUsers());
    }

    // File based tests to go here
}