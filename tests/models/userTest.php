<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers User
 */
final class UserTest extends TestCase
{
    public function testCanCreateUser()
    {
        $this->assertInstanceOf(
            User::class,
            new User("firstname", "surname")
        );
    }

    public function testValidateUserValid()
    {
        $user = new User("firstname", "surname");
        $this->assertEquals($user->isValid(), true);
    }
    public function testValidateUserMissingFirstname()
    {
        $user = new User(null, "surname");
        $this->assertEquals($user->isValid(), false);
    }

    public function testValidateUserMissingSurname()
    {
        $user = new User("firstname", null);
        $this->assertEquals($user->isValid(), false);
    }


}