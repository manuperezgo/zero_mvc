<?php

class UsersController
{
    public function __construct()
    {
        $usersInstance = Users::getInstance();
        $usersInstance->getUsers();
    }

    /**
     * @return array User
     */
    public function index()
    {
        $usersInstance = Users::getInstance();
        $users = $usersInstance->getUsers();
        ob_start();
        extract($users);
        require_once(__DIR__."/../views/edit.php");
        echo ob_get_clean();
    }

    public function edit()
    {
        $usersPostData = $_POST['users'];

        $users = [];
        foreach($usersPostData as $index => $userData){
            $user = new User($userData['firstname'], $userData['surname']);

            if($user->isValid()) {
                $users[] = $user;
            }
        }
        $userInstance = Users::getInstance();
        $userInstance->setUsers($users)->save();

        header("Location: /");
    }
}